A Vue component to convert JSON responses and requests to YAML format compatible with Swagger.

## Get started

Install:

```bash
npm install json2swagger
```

Then, import and register the component:

```html
<template>
    <Json2Swagger :request="true" />
</template>

<script>
import Json2Swagger from 'json2swagger';

export default {
    name: 'MyPage',
    components: {
        Json2Swagger
    }
}
</script>
```

## Is it production-ready? Any improvements?

- [x] Base Json to YAML convert
- [x] Json to Swagger Components
- [x] Send request and convert to Components
- [x] Send request and convert it to Path
- [x] Copy request from Google Chrome and convert it to Path
- [ ] Work with descriptions...
- [ ] Moving nested structures into a separate definition
- [ ] Authorization header to Security Scheme
- [ ] ...

## License

[MIT](https://github.com/sagalbot/vue-select/blob/master/LICENSE.md)
