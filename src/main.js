import Vue from 'vue'
import App from './App.vue'
import Json2Swagger from './index';

Vue.config.productionTip = false

Vue.use(Json2Swagger);

new Vue({
  render: h => h(App),
}).$mount('#app')
