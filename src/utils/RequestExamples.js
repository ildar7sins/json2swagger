/**
 * Site with fake API: https://jsonplaceholder.typicode.com/
 */

export default {
    'GET': {
        method: 'GET',
        url: 'https://jsonplaceholder.typicode.com/todos?id=1',
        headers: [
            { key: 'Authorization', value: 'Bearer 9vzzAGdm6ETws9wXv' },
            { key: '', value: '' },
        ],
        requestPayload: null,
    }
}