import { ARR, BOOL, INT, NULL, OBJ, STR } from '../../utils/Types';

export default {
    methods: {
        isJsonValid (json, name = '') {
            try {
                JSON.parse(json);
            } catch (e) {
                // @TODO: Add error notice
                console.error(`${name} is not valid JSON!`)
                return false;
            }
            return true;
        },
        isSimpleType (type) {
            return ![ARR, OBJ].includes(type)
        },
        getType (value) {
            if (typeof value === 'string') {
                return STR;
            } else if (typeof value === 'number') {
                return INT;
            } else if (typeof value === 'boolean') {
                return BOOL;
            } else if (Array.isArray(value)) {
                return ARR;
            } else if (value === null) {
                return NULL;
            } else if (typeof value === 'object') {
                return OBJ;
            }
            
            return 'UnknownType';
        },
        parseArray (arr, items) {
            for (const item of arr) {
                const type = this.getType(item);
                const newItem = {
                    type,
                    // TODO: Прикрутить описание...
                    // description: ''
                };
                
                if (this.isSimpleType(type)) {
                // TODO: Do smth...
                } else if (type === OBJ) {
                    newItem.properties = {};
                    
                    this.parseObject(item, newItem.properties);
                } else if (type === ARR) {
                    newItem.items.oneOf = [];
                    
                    this.parseArray(item, newItem.items.oneOf);
                }
                
                items.push(newItem);
            }
        },
        clearArrayDuplicates (arr) {
            return Array.from(new Set(arr.map(obj => JSON.stringify(obj)))).map(str => JSON.parse(str));
            
            // @FIXME: Refactor with moving objects to components
            // if (items.length === 1) {
            //     return [items[0]];
            // } else {
                // return items;
            // }
        },
        parseObject (obj, properties) {
            for (const [key, value] of Object.entries(obj)) {
                const type = this.getType(value);
                
                if (this.isSimpleType(type)) {
                    // TODO: Прикрутить описание...
                    properties[key] = { type };
                } else if (type === OBJ) {
                    // TODO: Прикрутить описание...
                    properties[key] = { type, properties: {} };
                    
                    this.parseObject(value, properties[key].properties);
                } else if (type === ARR) {
                    // TODO: Прикрутить описание...
                    properties[key] = { type, items: { oneOf: [] } };
                    
                    this.parseArray(value, properties[key].items.oneOf);
                    properties[key].items.oneOf = this.clearArrayDuplicates(properties[key].items.oneOf);
                }
            }
        },
    }
};
